from django.shortcuts import render, redirect, get_list_or_404
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm

# from receipts.forms import


# Create your views here.
@login_required
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {"receipt_list": receipts}
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    
    else:
        form = ReceiptForm()
    
    context = {
        "form": form,
    }

    return render(request, "receipts/create.html", context)


@login_required
def view_expenses(request):
    expenses = ExpenseCategory.objects.filter(owner=request.user)
    context = {"expense_list":expenses}
    return render(request,"receipts/expenses.html", context)

@login_required
def view_accounts(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {"account_list":accounts}
    return render(request,"receipts/accounts.html", context)

@login_required
def create_expense(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            expense = form.save(commit=False)
            expense.owner = request.user
            expense.save()
            return redirect("category_list")
    
    else:
        form = ExpenseCategoryForm()
    
    context = {
        "form": form,
    }

    return render(request, "receipts/createExpense.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(commit=False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    
    else:
        form = AccountForm()
    
    context = {
        "form": form,
    }

    return render(request, "receipts/createAccount.html", context)
